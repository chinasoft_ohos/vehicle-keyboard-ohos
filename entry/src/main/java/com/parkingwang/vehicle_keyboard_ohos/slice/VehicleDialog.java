package com.parkingwang.vehicle_keyboard_ohos.slice;

import com.parkingwang.keyboard.OnInputChangedListener;
import com.parkingwang.keyboard.PopupKeyboard;
import com.parkingwang.keyboard.view.InputView;
import com.parkingwang.vehicle_keyboard_ohos.ResourceTable;
import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.LayoutScatter;
import ohos.agp.window.dialog.CommonDialog;
import ohos.agp.window.service.DisplayAttributes;
import ohos.agp.window.service.DisplayManager;
import ohos.app.Context;

/**
 * @author 黄浩杭
 * @since 2018-12-14
 */
public class VehicleDialog extends CommonDialog {
    private Context context;

    public VehicleDialog(Context context) {
        super(context);
        this.context = context;
        init();
    }

    @Override
    protected void onShow() {
        super.onShow();
    }

    @Override
    protected void onCreate() {
        super.onCreate();
    }

    private void init() {
        Component component = LayoutScatter.getInstance(context)
                .parse(ResourceTable.Layout_dialog_vehicle, null, false);
        setContentCustomComponent(component);
        setCornerRadius(10f);
        DisplayAttributes displayAttributes = DisplayManager.getInstance()
                .getDefaultDisplay(context).get().getAttributes();
        int width = displayAttributes.width;
        setSize((int)(width * 0.9), ComponentContainer.LayoutConfig.MATCH_CONTENT);
        component.findComponentById(ResourceTable.Id_ok).setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                destroy();
            }
        });
        final InputView mInputView = (InputView)component.findComponentById(ResourceTable.Id_input_view);

        // 创建弹出键盘
        final PopupKeyboard mPopupKeyboard = new PopupKeyboard(context);

        // 弹出键盘内部包含一个KeyboardView，在此绑定输入两者关联。
        mPopupKeyboard.attach(mInputView, this);

        // 隐藏确定按钮
        mPopupKeyboard.getKeyboardEngine().setHideOKKey(false);

        // KeyboardInputController提供一个默认实现的新能源车牌锁定按钮
        mPopupKeyboard.getController().setDebugEnabled(true);
        mPopupKeyboard.getController().addOnInputChangedListener(new OnInputChangedListener() {
            @Override
            public void onChanged(String number, boolean isCompleted) {

            }

            @Override
            public void onCompleted(String number, boolean isAutoCompleted) {
                if (!isAutoCompleted) {
                    destroy();
                }
            }
        });
        mInputView.performFirstFieldView();
    }

    @Override
    public void show() {
        super.show();
    }

}
