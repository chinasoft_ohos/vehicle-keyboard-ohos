# vehicle-keyboard-ohos

#### 项目介绍
- 项目名称：vehicle-keyboard-ohos
- 所属系列：openharmony的第三方组件适配移植
- 功能：快速输入车牌号
- 项目移植状态：主功能完成
- 调用差异：无
- 开发版本：sdk6，DevEco Studio2.2 Beta1
- 基线版本：Release 0.7.4

#### 效果演示
![输入图片说明](https://gitee.com/chinasoft_ohos/vehicle-keyboard-ohos/raw/master/img/demo.gif "demo.gif")


#### 安装教程
1.在项目根目录下的build.gradle文件中，
 ```gradle
allprojects {
    repositories {
        maven {
            url 'https://s01.oss.sonatype.org/content/repositories/releases/'
        }
    }
}
 ```

2.在entry模块的build.gradle文件中，
 ```gradle
 dependencies {
    implementation('com.gitee.chinasoft_ohos:vehicle-keyboard-ohos:1.0.0')
    ......  
 }
 ```

在sdk6，DevEco Studio2.2 Beta1下项目可直接运行
如无法运行，删除项目.gradle,.idea,build,gradle,build.gradle文件，
并依据自己的版本创建新项目，将新项目的对应文件复制到根目录下

#### 使用说明

- 支持中华人民共和国现行绝大部分车牌号码类型：
- 民用蓝牌、黄牌
- 新能源车牌
- 港澳车牌
- 武警车牌
- 军队车牌
- 新旧大使馆车牌
- 新旧领事馆车牌
- 民航车牌

 1.在XML中放置车牌输入组件：

   ```xml
       <com.parkingwang.keyboard.view.InputView
            ohos:id="$+id:inputView"
            ohos:height="60vp"
            ohos:width="match_parent"
            ohos:top_margin="20vp"
            ohos:bottom_margin="20vp"
            ohos:left_margin="15vp"
            ohos:right_margin="15vp"
            />
   ```

2.在代码中绑定输入组件与键盘的关联：

  **使用弹出键盘**

  ```
  // Init Views

  // 创建弹出键盘
  mPopupKeyboard = new PopupKeyboard(this);
  // 弹出键盘内部包含一个KeyboardView，在此绑定输入两者关联。
  mPopupKeyboard.attach(mInputView, this);

  // KeyboardInputController提供一个默认实现的新能源车牌锁定按钮
  mPopupKeyboard.getController()
          .setDebugEnabled(true)
          .bindLockTypeProxy(new KeyboardInputController.ButtonProxyImpl(lockTypeButton) {
              @Override
              public void onNumberTypeChanged(boolean isNewEnergyType) {
                  super.onNumberTypeChanged(isNewEnergyType);
                  if (isNewEnergyType) {
                      changeNewEnergy.setTextColor(new Color(Color.getIntColor("#ff99cc00")));
                  } else {
                      changeNewEnergy.setTextColor(new Color(Color.getIntColor("#ff000000")));
                  }
              }
          });
  ```

  **不弹出键盘，直接显示**

  ```

  // 使用 KeyboardInputController 来关联
  mController = KeyboardInputController
                      .with(mKeyboardView, inputView);

  mController.useDefaultMessageHandler();
  ```

  **切换新能源和普通车牌是否校验**

  ```
  mController.setSwitchVerify(false) //不校验（默认校验）
  ```

#### 键盘功能特性设置

1. 设置是否显示“确定”键

根据需要，你可以通过调用KeyboardEngine的`setHideOKKey(boolean)`来设置是否隐藏“确定”键。

```
mPopupKeyboard.getKeyboardEngine().setHideOKKey(mHideOKKey);
```

2. 优先显示周边省份

根据需要，在不同地区的用户，输入车牌号码时，可以根据当地地理位置，显示周边省份的简称。
获取地理位置需要的定位功能，需要你外部调用定位API，获取到对应的省份名称后，设置到KeyboardEngine中。
使用如下代码：

```
mPopupKeyboard.getKeyboardEngine().setLocalProvinceName("广东省");
```

3. 设置键盘按下时的气泡：

(1). 正确地显示气泡

由于顶层按键的气泡会显示到键盘之外，因此需要键盘所在的父布局代码中增加以下属性设置（如果气泡范围超出父布局，则需往上递归设置）：

```
    component.setClipEnabled(false);
    mKeyboardView.setShowBubble(true);
```

(2). 不显示气泡

```
    mKeyboardView.setShowBubble(false);
```

#### 键盘样式设置

1. 设置键盘按钮文字大小

在Java代码中添加以下设置：
```
    mKeyboardView.setCNTextSize(float); //设置中文字体大小
    mKeyboardView.setENTextSize(float); //设置英文字母或数字字体大小
```

2. 设置输入组件字体大小：

```
    <com.parkingwang.keyboard.view.InputView
            app:pwkInputTextSize="22fp"
            ..../>
```

#### 测试信息

CodeCheck代码测试无异常

CloudTest代码测试无异常

病毒安全检测通过

当前版本demo功能与原组件基本无差异


#### 版本迭代

- 1.0.0

#### 版权和许可信息
```
Copyright (c) 2017 Xi'an iRain IoT. Technology Service CO., Ltd.

注意：于2018年04月08日，本源代码修改开源协议，声明如下：

西安艾润物联公司版本所有，保留所有版权。
本源代码仅供技术学习交流，本项目及衍生代码均不可作商业使用。
```