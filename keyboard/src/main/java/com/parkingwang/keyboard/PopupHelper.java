package com.parkingwang.keyboard;

import com.parkingwang.keyboard.annotation.NonNull;
import com.parkingwang.keyboard.view.KeyboardView;
import ohos.aafwk.ability.Ability;
import ohos.agp.components.*;
import ohos.agp.utils.LayoutAlignment;
import ohos.agp.window.service.Window;
import ohos.app.Context;

/**
 * @author 黄浩杭 (huanghaohang@parkingwang.com)
 * @version 0.4.1
 * @since 2017-11-03 0.1
 */
public class PopupHelper {

    public static boolean showToActivity(final Ability activity, ComponentContainer componentContainer, final KeyboardView keyboardView) {
        return showToWindow(componentContainer, activity.getWindow(), keyboardView, false);
    }

    public static boolean showToWindow(final Window window, ComponentContainer componentContainer, final KeyboardView keyboardView) {
        return showToWindow(componentContainer, window, keyboardView, false);
    }

    public static boolean showToWindow(ComponentContainer rootView, final Window window, final KeyboardView keyboardView, boolean belowFirstView) {
        StackLayout keyboardWrapper = (StackLayout) rootView.findComponentById(ResourceTable.Integer_keyboard_wrapper_id);

        if (keyboardWrapper == null) {
            ComponentParent keyboardViewParent = keyboardView.getComponentParent();
            if (keyboardViewParent != null) {
                if (((Component) keyboardViewParent).getId() == ResourceTable.Integer_keyboard_wrapper_id
                        && keyboardViewParent instanceof StackLayout) {
                    keyboardWrapper = (StackLayout) keyboardViewParent;
                    makeSureHasNoParent(keyboardWrapper);
                }
            }
            if (keyboardWrapper == null) {
                keyboardWrapper = wrapKeyboardView(keyboardView.getContext(), keyboardView);
            }

            if (rootView instanceof StackLayout) {
                StackLayout.LayoutConfig params = new StackLayout.LayoutConfig(
                        ComponentContainer.LayoutConfig.MATCH_PARENT, ComponentContainer.LayoutConfig.MATCH_PARENT);
                params.alignment = LayoutAlignment.BOTTOM;
                if (belowFirstView) {
                    params.setMarginTop(rootView.getComponentAt(0).getHeight());
                }
                (rootView).addComponent(keyboardWrapper, params);
                keyboardView.renderLayout();
                keyboardView.invalidate();
            }

            if (rootView instanceof DirectionalLayout) {
                DirectionalLayout.LayoutConfig params = new DirectionalLayout.LayoutConfig(
                        ComponentContainer.LayoutConfig.MATCH_PARENT, ComponentContainer.LayoutConfig.MATCH_CONTENT);
                rootView.addComponent(keyboardWrapper, params);
                keyboardView.renderLayout();
                keyboardView.invalidate();
            }
            return true;
        } else {
            keyboardWrapper.setVisibility(Component.VISIBLE);
            keyboardWrapper.moveChildToFront(keyboardView.getComponentAt(0));
            return false;
        }
    }

    @NonNull
    private static StackLayout wrapKeyboardView(Context context, KeyboardView keyboardView) {
        StackLayout keyboardWrapper = new StackLayout(context);
        keyboardWrapper.setId(ResourceTable.Integer_keyboard_wrapper_id);
        keyboardWrapper.setClipEnabled(false);

        StackLayout.LayoutConfig keyboardParams = new StackLayout.LayoutConfig(
                ComponentContainer.LayoutConfig.MATCH_PARENT, ComponentContainer.LayoutConfig.MATCH_CONTENT, LayoutAlignment.BOTTOM);
        keyboardWrapper.addComponent(keyboardView, keyboardParams);
        return keyboardWrapper;
    }

    private static void makeSureHasNoParent(Component view) {
        if (view.getComponentParent() != null) {
            view.getComponentParent().removeComponent(view);
        }
    }

    public static boolean dismissFromActivity(Ability activity, ComponentContainer rootView) {
        return dismissFromWindow(activity.getWindow(), rootView);
    }

    public static boolean dismissFromWindow(Window window, ComponentContainer rootView) {
        Component view = rootView.findComponentById(ResourceTable.Integer_keyboard_wrapper_id);
        if (view == null) {
            return false;
        } else {
            view.getComponentParent().removeComponent(view);
            return true;
        }
    }
}
