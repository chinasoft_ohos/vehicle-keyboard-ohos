package com.parkingwang.keyboard;

import com.parkingwang.keyboard.annotation.ColorInt;
import com.parkingwang.keyboard.engine.KeyboardEngine;
import com.parkingwang.keyboard.view.InputView;
import com.parkingwang.keyboard.view.KeyboardView;
import ohos.aafwk.ability.Ability;

import ohos.agp.components.ComponentContainer;
import ohos.agp.components.element.StateElement;
import ohos.agp.window.dialog.CommonDialog;
import ohos.agp.window.service.Window;
import ohos.app.Context;

/**
 * @author Yoojia Chen (yoojiachen@gmail.com)
 * @version 2017-11-03 0.1
 * @since 2017-11-03 0.1
 */
public class PopupKeyboard {

    private final KeyboardView mKeyboardView;

    private KeyboardInputController mController;

    private boolean isDialog = false;

    private boolean isShown = false;

    public PopupKeyboard(Context context) {
        mKeyboardView = new KeyboardView(context);
    }

    public PopupKeyboard(Context context, @ColorInt int bubbleTextColor, StateElement okKeyBackgroundColor) {
        mKeyboardView = new KeyboardView(context);
        mKeyboardView.setBubbleTextColor(bubbleTextColor);
        mKeyboardView.setOkKeyTintColor(okKeyBackgroundColor);
    }

    public KeyboardView getKeyboardView() {
        return mKeyboardView;
    }

    public void attach(InputView inputView, final Ability activity, ComponentContainer componentContainer) {
        isDialog = false;
        attach(inputView, activity.getWindow(), componentContainer);
    }

    public void attach(InputView inputView, final CommonDialog dialog) {
        isDialog = true;
        attach(inputView, dialog.getWindow(), (ComponentContainer) dialog.getContentCustomComponent());
    }

    private void attach(InputView inputView, final Window window, ComponentContainer componentContainer) {
        if (mController == null) {
            mController = KeyboardInputController
                    .with(mKeyboardView, inputView);
            mController.useDefaultMessageHandler();

            inputView.addOnFieldViewSelectedListener(index -> show(window,componentContainer));
        }
    }

    public KeyboardInputController getController() {
        return checkAttachedController();
    }

    public KeyboardEngine getKeyboardEngine() {
        return mKeyboardView.getKeyboardEngine();
    }

    public void show(Ability activity, ComponentContainer componentContainer) {
        show(activity.getWindow(), componentContainer);
    }

    public void show(Window window, ComponentContainer componentContainer) {
        checkAttachedController();
        isShown = true;
        PopupHelper.showToWindow(componentContainer, window, mKeyboardView, isDialog);
    }

    public void dismiss(Ability activity,ComponentContainer rootView) {
        dismiss(activity.getWindow(),rootView);
    }

    public void dismiss(Window window,ComponentContainer rootView) {
        isShown = false;
        checkAttachedController();
        PopupHelper.dismissFromWindow(window,rootView);
    }

    public boolean isShown() {
        return isShown;
    }

    private KeyboardInputController checkAttachedController() {
        if (mController == null) {
            throw new IllegalStateException("Try attach() first");
        }
        return mController;
    }
}
