/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.parkingwang.keyboard;

/**
 * 属性常量
 * @since 2021-03-01
 */
public class AttrFieldName {
    /*
     *KeyboardView使用
     */
    public static final String KEYBOARDVIEW_PWBBUBBLECOLOR = "pwkBubbleColor";

    /**
     * InputView使用
     */
    public static final String INPUTVIEW_PWKINPUTTEXTSIZE = "InputView_pwkInputTextSize";
    public static final String INPUTVIEW_PWKSELECTEDDRAWABLE = "InputView_pwkSelectedDrawable";
    public static final String INPUTVIEW_PWKITEMBORDERSELECTEDCOLOR = "InputView_pwkItemBorderSelectedColor";

    private AttrFieldName() {
    }
}
