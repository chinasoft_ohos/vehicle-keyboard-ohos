/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.parkingwang.keyboard;

import ohos.global.resource.Element;
import ohos.app.Context;
import ohos.global.resource.NotExistException;
import ohos.global.resource.WrongTypeException;
import ohos.hiviewdfx.HiLog;
import ohos.hiviewdfx.HiLogLabel;

import java.io.IOException;

/**
 * 资源获取管理类
 *
 * @since 2021-03-01
 */
public class HmsResourcesManager {
    private static HiLogLabel hiLogLabel = new HiLogLabel(HiLog.LOG_APP, 0x78002, HmsResourcesManager.class.getSimpleName());

    private HmsResourcesManager() {
    }

    /**
     * 获取资源float值
     * @param context 上下文
     * @param resourcesId 资源id
     * @return 资源float值
     */
    public static float getFloatValueByResources(Context context, int resourcesId) {
        Element element = get(context, resourcesId);
        if (element != null) {
            try {
                return element.getFloat();
            } catch (NotExistException | WrongTypeException | IOException e) {
                HiLog.info(hiLogLabel, "getFloatValueByResources err exception = " + e);
            }
        }
        return 0;
    }

    /**
     * 获取资源color值
     * @param context 上下文
     * @param resourcesId 资源id
     * @return 资源color值
     */
    public static int getColorValueByResources(Context context, int resourcesId) {
        Element element = get(context, resourcesId);
        if (element != null) {
            try {
                return element.getColor();
            } catch (NotExistException | WrongTypeException | IOException e) {
                HiLog.info(hiLogLabel, "getColorValueByResources err exception = " + e);
            }
        }
        return 0;
    }

    /**
     * 获取资源String值
     * @param context 上下文
     * @param resourcesId 资源id
     * @return 资源String值
     */
    public static String getStringValueByResources(Context context, int resourcesId) {
        Element element = get(context, resourcesId);
        if (element != null) {
            try {
                return element.getString();
            } catch (NotExistException | WrongTypeException | IOException e) {
                HiLog.info(hiLogLabel, "getColorValueByResources err exception = " + e);
            }
        }
        return "";
    }

    /**
     * 获取资源Element
     * @param context 上下文
     * @param resourcesId 资源id
     * @return Element
     */
    private static Element get(Context context, int resourcesId) {
        Element element = null;
        try {
            element = context.getResourceManager().getElement(resourcesId);
        } catch (NotExistException | WrongTypeException | IOException e) {
            HiLog.info(hiLogLabel, "get err exception = " + e);
        }
        return element;
    }
}
