/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.parkingwang.keyboard;

import ohos.agp.components.Attr;
import ohos.agp.components.AttrSet;
import ohos.agp.utils.Color;
import ohos.hiviewdfx.HiLog;
import ohos.hiviewdfx.HiLogLabel;

/**
 * 获取自定义属性工具类,如果没有配置这个自定义属性则使用默认值
 *
 * @since V1.0
 */
public class AttrUtils {
    private static HiLogLabel hiLogLabel = new HiLogLabel(HiLog.LOG_APP, 0x78002, AttrUtils.class.getSimpleName());

    private AttrUtils() {
    }

    /**
     * 获取属性Float值
     *
     * @param attrSet 属性集合
     * @param filedName 属性名称
     * @param defaultValue 默认值
     * @return 属性Float值
     */
    public static float getFloatValueByAttr(AttrSet attrSet, String filedName, float defaultValue) {
        Attr attr = get(attrSet, filedName);
        if (attr != null) {
            return attr.getFloatValue();
        }
        return defaultValue;
    }

    /**
     * 获取属性int值
     *
     * @param attrSet 属性集合
     * @param filedName 属性名称
     * @param defaultValue 默认值
     * @return 属性int值
     */
    public static int getIntValueByAttr(AttrSet attrSet, String filedName, int defaultValue) {
        Attr attr = get(attrSet, filedName);
        if (attr != null) {
            return attr.getIntegerValue();
        }
        return defaultValue;
    }

    /**
     * 获取属性颜色对象
     *
     * @param attrSet 属性集合
     * @param filedName 属性名称
     * @param defaultValue 默认值
     * @return 属性颜色
     */
    public static Color getColorValueByAttr(AttrSet attrSet, String filedName, Color defaultValue) {
        Attr attr = get(attrSet, filedName);
        if (attr != null) {
            return attr.getColorValue();
        }
        HiLog.info(hiLogLabel,"getColorValueByAttr return defaultValue");
        return defaultValue;
    }

    /**
     * 获取属性int值
     *
     * @param attrSet 属性集合
     * @param filedName 属性名称
     * @param defaultValue 默认值
     * @return 属性int值
     */
    public static int getDimensionValueByAttr(AttrSet attrSet, String filedName, int defaultValue) {
        Attr attr = get(attrSet, filedName);
        if (attr != null) {
            return attr.getDimensionValue();
        }
        return defaultValue;
    }

    /**
     * 获取属性字符串
     *
     * @param attrSet 属性集合
     * @param filedName 属性名称
     * @param defaultValue 默认值
     * @return 属性字符串值
     */
    public static String getStringValueByAttr(AttrSet attrSet, String filedName, String defaultValue) {
        Attr attr = get(attrSet, filedName);
        if (attr != null) {
            return attr.getStringValue();
        }
        return defaultValue;
    }

    /**
     * 获取属性布尔值
     *
     * @param attrSet 属性集合
     * @param filedName 属性名称
     * @param defaultValue 默认值
     * @return 属性布尔值
     */
    public static boolean getBooleanValueByAttr(AttrSet attrSet, String filedName, boolean defaultValue) {
        Attr attr = get(attrSet, filedName);
        if (attr != null) {
            return attr.getBoolValue();
        }
        return defaultValue;
    }

    /**
     * 获取属性
     *
     * @param attrSet 属性集合
     * @param filedName 属性名称
     * @return 具体属性名称对应的属性
     */
    private static Attr get(AttrSet attrSet, String filedName) {
        if (attrSet != null && attrSet.getAttr(filedName).isPresent()) {
            try {
                return attrSet.getAttr(filedName).get();
            } catch (Exception e) {
                HiLog.info(hiLogLabel, "test get fail need error = " + e);
                return null;
            }
        }
        return null;
    }
}
