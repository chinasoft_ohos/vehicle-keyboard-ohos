package com.parkingwang.keyboard.view;

import com.parkingwang.keyboard.ResourceTable;
import ohos.agp.components.Button;
import ohos.agp.components.Component;
import ohos.agp.components.Image;
import ohos.agp.components.Text;
import ohos.agp.utils.Color;
import ohos.agp.utils.TextTool;
import ohos.hiviewdfx.HiLog;
import ohos.hiviewdfx.HiLogLabel;

import java.util.ArrayList;
import java.util.List;

/**
 * @author 陈小锅 (yoojiachen@gmail.com)
 */
abstract class FieldViewGroup {

    private static final HiLogLabel TAG = new HiLogLabel(HiLog.LOG_APP,0x78002,FieldViewGroup.class.getSimpleName());

    private final InputViewButton[] mFieldViews = new InputViewButton[8];

    public FieldViewGroup() {
        final int[] resIds = new int[]{
                ResourceTable.Id_number_0,
                ResourceTable.Id_number_1,
                ResourceTable.Id_number_2,
                ResourceTable.Id_number_3,
                ResourceTable.Id_number_4,
                ResourceTable.Id_number_5,
                ResourceTable.Id_number_6,
                ResourceTable.Id_number_7,
        };
        for (int i = 0; i < resIds.length; i++) {
            mFieldViews[i] = (InputViewButton) findViewById(resIds[i]);
            mFieldViews[i].setTag("[RAW.idx:" + i + "]");
        }
        // 默认时，显示8位
        changeTo8Fields();
    }

    protected abstract Button findViewById(int id);

    public void setTextToFields(String text) {
        Image image;
        // cleanup
        for (Button f : mFieldViews) {
            f.setText(null);
        }

        final char[] chars = text.toCharArray();
        if (chars.length >= 8) {
            changeTo8Fields();
        } else {
            changeTo7Fields();
        }

        // 显示到对应键位
        final Button[] fields = getAvailableFields();
        for (int i = 0; i < fields.length; i++) {
            final String txt;
            if (i < chars.length) {
                txt = String.valueOf(chars[i]);
            } else {
                txt = null;
            }
            fields[i].setText(txt);
        }
    }

    public Button[] getAvailableFields() {
        final List<Button> output = new ArrayList<>(8);
        final int lastIndex = mFieldViews.length - 1;
        Button fieldView;
        for (int i = 0; i < mFieldViews.length; i++) {
            fieldView = mFieldViews[i];
            if (i != lastIndex || fieldView.getVisibility() == Component.VISIBLE) {
                output.add(fieldView);
            }
        }
        return output.toArray(new Button[output.size()]);
    }

    public Button getFieldAt(int index) {
        return mFieldViews[index];
    }

    public boolean changeTo7Fields() {
        if (mFieldViews[7].getVisibility() != Component.VISIBLE) {
            return false;
        }
        mFieldViews[7].setVisibility(Component.HIDE);
        mFieldViews[7].setText(null);
        for (InputViewButton mFieldView : mFieldViews) {
            mFieldView.invalidate();
        }
        return true;
    }

    public boolean changeTo8Fields() {
        if (mFieldViews[7].getVisibility() == Component.VISIBLE) {
            return false;
        }
        mFieldViews[7].setVisibility(Component.VISIBLE);
        mFieldViews[7].setText(null);
        for (InputViewButton mFieldView : mFieldViews) {
            mFieldView.invalidate();
        }
        return true;
    }

    public Button getLastField() {
        if (mFieldViews[7].getVisibility() == Component.VISIBLE) {
            return mFieldViews[7];
        } else {
            return mFieldViews[6];
        }
    }

    public Button getFirstSelectedFieldOrNull() {
        for (Button field : getAvailableFields()) {
            if (field.isSelected()) {
                return field;
            }
        }
        return null;
    }

    public Button getLastFilledFieldOrNull() {
        final Button[] fields = getAvailableFields();
        for (int i = fields.length - 1; i >= 0; i--) {
            if (!TextTool.isNullOrEmpty(fields[i].getText())) {
                return fields[i];
            }
        }
        return null;
    }

    public Button getFirstEmptyField() {
        final Button[] fields = getAvailableFields();
        Button out = fields[0];
        for (Button field : fields) {
            out = field;
            final CharSequence keyTxt = field.getText();
            if (TextTool.isNullOrEmpty(keyTxt)) {
                break;
            }
        }
        HiLog.info(TAG, "[-- CheckEmpty --]: Btn.idx: " + out.getTag() + ", Btn.text: " + out.getText() + ", Btn.addr: " + out);
        return out;
    }

    public int getNextIndexOfField(Button target) {
        final Button[] fields = getAvailableFields();
        for (int i = 0; i < fields.length; i++) {
            if (target == fields[i]) {
                return Math.min(fields.length - 1, i + 1);
            }
        }
        return 0;
    }

    public boolean isAllFieldsFilled() {
        for (Button field : getAvailableFields()) {
            if (TextTool.isNullOrEmpty(field.getText())) {
                return false;
            }
        }
        return true;
    }

    public String getText() {
        final StringBuilder sb = new StringBuilder();
        for (Button field : getAvailableFields()) {
            sb.append(field.getText());
        }
        return sb.toString();
    }

    public void setupAllFieldsTextSize(float size) {
        for (Button field : mFieldViews) {
            field.setTextSize((int)size, Text.TextSizeType.FP);
        }
    }

    public void setupAllFieldsOnClickListener(Component.ClickedListener listener) {
        for (Button field : mFieldViews) {
            field.setClickedListener(listener);
        }
    }
}
