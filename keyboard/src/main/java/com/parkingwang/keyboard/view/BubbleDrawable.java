package com.parkingwang.keyboard.view;

import com.parkingwang.keyboard.HmsResourcesManager;
import com.parkingwang.keyboard.ResourceTable;
import com.parkingwang.keyboard.Texts;
import com.parkingwang.keyboard.annotation.NonNull;
import com.parkingwang.keyboard.utils.Utils;
import ohos.agp.components.element.PixelMapElement;
import ohos.agp.components.element.ShapeElement;
import ohos.agp.render.Canvas;
import ohos.agp.render.ColorFilter;
import ohos.agp.render.Paint;
import ohos.agp.utils.Color;
import ohos.agp.utils.TextAlignment;
import ohos.app.Context;
import org.jetbrains.annotations.Nullable;

/**
 * @author 黄浩杭 (huanghaohang@parkingwang.com)
 * @version 0.1
 */
public class BubbleDrawable extends ShapeElement {

    private static final float ANCHOR_Y = 103f / 118;
    private static final float TEXT_CENTER_Y = 41f / 118;

    private final PixelMapElement mBackgroundDrawable;
    private final Paint mTextPaint = new Paint();

    private final float mCNTextSize;
    private final float mENTextSize;

    private String mText;

    public BubbleDrawable(Context context) {
        mBackgroundDrawable = Utils.getPixelMapElement(context, ResourceTable.Media_pwk_key_bubble_bg2);
        setBounds(0, 0, (int) (mBackgroundDrawable.getWidth() * 0.7), (int) (mBackgroundDrawable.getHeight() * 0.7));
        mCNTextSize = HmsResourcesManager.getFloatValueByResources(context, ResourceTable.Float_pwk_keyboard_key_cn_text_size);
        mENTextSize = HmsResourcesManager.getFloatValueByResources(context, ResourceTable.Float_pwk_keyboard_key_en_text_size);
        mTextPaint.setColor(new Color(HmsResourcesManager.getColorValueByResources(context, ResourceTable.Color_pwk_keyboard_primary_color)));
        mTextPaint.setTextAlign(TextAlignment.CENTER);
        mTextPaint.setFakeBoldText(true);
    }

    public void setTextColor(int color) {
        mTextPaint.setColor(new Color(color));
    }

    public void setText(String text) {
        mText = text;
        if (Texts.isEnglishLetterOrDigit(text)) {
            mTextPaint.setTextSize((int) mENTextSize);
        } else {
            mTextPaint.setTextSize((int) mCNTextSize);
        }
    }

    public void draw(@NonNull Canvas canvas) {
        canvas.save();
        canvas.translate(0, (1 - ANCHOR_Y) * getIntrinsicHeight());
        mBackgroundDrawable.drawToCanvas(canvas);
        final float textCenterX = getIntrinsicWidth() / 2f;
        final float textCenterY = getIntrinsicHeight() * TEXT_CENTER_Y - (mTextPaint.ascent() + mTextPaint.descent()) / 2;
        canvas.drawText(mTextPaint, mText, textCenterX, textCenterY);
        canvas.restore();
    }

    public void setAlpha(int alpha) {
        mBackgroundDrawable.setAlpha(alpha);
        mTextPaint.setAlpha(alpha);
    }

    public void setColorFilter(@Nullable ColorFilter colorFilter) {
        mTextPaint.setColorFilter(colorFilter);
    }

    public int getOpacity() {
        return 0;
    }

    public int getIntrinsicWidth() {
        return mBackgroundDrawable.getWidth();
    }

    public int getIntrinsicHeight() {
        return mBackgroundDrawable.getHeight();
    }

    public void setBounds(int left, int top, int right, int bottom) {
        mBackgroundDrawable.setBounds(left, top, right, bottom);
    }
}
