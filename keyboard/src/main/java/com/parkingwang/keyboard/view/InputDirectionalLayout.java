/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.parkingwang.keyboard.view;

import ohos.agp.components.AttrSet;
import ohos.agp.components.Component;
import ohos.agp.components.DirectionalLayout;
import ohos.app.Context;

public class InputDirectionalLayout extends DirectionalLayout {
    public InputDirectionalLayout(Context context) {
        super(context);
    }

    public InputDirectionalLayout(Context context, AttrSet attrSet) {
        super(context, attrSet);
    }

    public InputDirectionalLayout(Context context, AttrSet attrSet, String styleName) {
        super(context, attrSet, styleName);
    }

    public int getINLChildCount() {
        return getChildCount();
    }

    public Component getINLComponentAt(int index) {
        return getComponentAt(index);
    }
}
