/*
 * Copyright (c) 2017. Xi'an iRain IOT Technology service CO., Ltd (ShenZhen). All Rights Reserved.
 */

package com.parkingwang.keyboard.view;

import ohos.agp.components.Component;
import ohos.agp.components.DirectionalLayout;
import ohos.app.Context;

import java.util.Stack;

/**
 * @author 黄浩杭 (huanghaohang@parkingwang.com)
 * @since 2017-09-27 0.1
 */
final class KeyViewCacheHelper {
    private final Stack<KeyView> mKeyViews = new Stack<>();

    void recyclerKeyRows(KeyboardView keyboardView, int row) {
        int childCount = keyboardView.getChildCount();
        if (childCount > row) {
            for (int i = 0, trimCount = childCount - row; i < trimCount; i++) {
                trimKeyRowLayout(keyboardView, 0);
            }
        } else if (childCount < row) {
            for (int i = childCount; i < row; i++) {
                fixKeyRowLayout(keyboardView);
            }
        }
    }

    private void trimKeyRowLayout(KeyboardView keyboardView, int row) {
        KeyRowLayout keyRow = (KeyRowLayout) keyboardView.getComponentAt(row);
        int column = keyRow.getChildCount();
        final int targetIndex = 0;
        for (int j = 0; j < column; j++) {
            // 移除第index位之后，原来index+1位会变成index位
            pushKeyView((KeyView) keyRow.getComponentAt(targetIndex));
            keyRow.removeComponentAt(targetIndex);
        }
        keyboardView.removeComponent(keyRow);
    }

    private void fixKeyRowLayout(KeyboardView keyboardView) {
        DirectionalLayout.LayoutConfig params = new DirectionalLayout.LayoutConfig(
                DirectionalLayout.LayoutConfig.MATCH_PARENT,
                DirectionalLayout.LayoutConfig.MATCH_CONTENT);
        KeyRowLayout keyRowLayout = new KeyRowLayout(keyboardView.getContext());
        keyRowLayout.setLayoutConfig(params);
        keyRowLayout.setKeyBoardView(keyboardView);
        keyboardView.addComponent(keyRowLayout, 0);
    }

    void recyclerKeyViewsInRow(KeyRowLayout keyRow, int targetCount, Component.ClickedListener listener) {
        int childCount = keyRow.getChildCount();
        if (childCount < targetCount) {
            for (int i = childCount; i < targetCount; i++) {
                keyRow.addComponent(pullKeyView(keyRow.getContext(), listener));
            }
        } else if (childCount > targetCount) {
            trimKeyViews(keyRow, targetCount, childCount);
        }
    }

    private void trimKeyViews(KeyRowLayout keyRow, int targetCount, int childCount) {
        final int targetIndex = targetCount;
        for (int i = targetCount; i < childCount; i++) {
            // 移除第index位之后，原来index+1位会变成index位
            KeyView keyView = (KeyView) keyRow.getComponentAt(targetIndex);
            keyRow.removeComponentAt(targetIndex);
            pushKeyView(keyView);
        }
    }

    private KeyView pullKeyView(Context context, Component.ClickedListener listener) {
        if (mKeyViews.empty()) {
            KeyView keyView = new KeyView(context);
            keyView.setClickedListener(listener);
            DirectionalLayout.LayoutConfig params =
                    new DirectionalLayout.LayoutConfig(DirectionalLayout.LayoutConfig.MATCH_CONTENT,
                            DirectionalLayout.LayoutConfig.MATCH_PARENT);
            keyView.setLayoutConfig(params);
            return keyView;
        } else {
            return mKeyViews.pop();
        }
    }

    private void pushKeyView(KeyView keyView) {
        mKeyViews.push(keyView);
    }
}
