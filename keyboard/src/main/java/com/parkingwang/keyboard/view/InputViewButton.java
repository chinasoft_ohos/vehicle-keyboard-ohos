/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.parkingwang.keyboard.view;

import com.parkingwang.keyboard.AttrUtils;
import com.parkingwang.keyboard.HmsResourcesManager;
import com.parkingwang.keyboard.ResourceTable;
import ohos.agp.components.AttrSet;
import ohos.agp.components.Button;
import ohos.agp.components.Component;
import ohos.agp.render.Canvas;
import ohos.agp.render.Paint;
import ohos.agp.utils.Color;
import ohos.agp.utils.Point;
import ohos.app.Context;
import ohos.hiviewdfx.HiLog;
import ohos.hiviewdfx.HiLogLabel;

public class InputViewButton extends Button implements Component.DrawTask {
    private static HiLogLabel hiLogLabel = new HiLogLabel(HiLog.LOG_APP, 0x001, InputViewButton.class.getSimpleName());
    private String speedDirection = "null";
    private Paint paint;
    private float speedWidth = 1.0f;

    public InputViewButton(Context context) {
        super(context);
    }

    public InputViewButton(Context context, AttrSet attrSet) {
        super(context, attrSet);
        speedDirection = AttrUtils.getStringValueByAttr(attrSet, "speeddirection", "null");
        speedWidth = AttrUtils.getFloatValueByAttr(attrSet, "speedwidth", 1.0f);
        Color speedColor = AttrUtils.getColorValueByAttr(attrSet, "speedcolor", new Color(HmsResourcesManager.getColorValueByResources(context, ResourceTable.Color_pwk_border_gray)));
        if (paint == null) {
            paint = new Paint();
        }
        HiLog.info(hiLogLabel, "construct two field speedDirection = " + speedDirection
                + " speedwidth = " + speedWidth);
        paint.setColor(speedColor);
        paint.setStrokeWidth(speedWidth);
        addDrawTask(this);
    }

    public InputViewButton(Context context, AttrSet attrSet, String styleName) {
        super(context, attrSet, styleName);
    }

    @Override
    public void onDraw(Component component, Canvas canvas) {
        int height = getHeight();
        int width = getWidth();
        Point startPoint = null;
        Point endPoint = null;
        if ("left".equals(speedDirection)) {
            startPoint = new Point(0, 0);
            endPoint = new Point(0, height);
        } else if ("right".equals(speedDirection)) {
            startPoint = new Point(width - speedWidth, 0);
            endPoint = new Point(width - speedWidth, height);
        } else if ("top".equals(speedDirection)) {
            startPoint = new Point(0, 0);
            endPoint = new Point(width, 0);
        } else if ("bottom".equals(speedDirection)) {
            startPoint = new Point(0, height - speedWidth);
            endPoint = new Point(width, height - speedWidth);
        }
        if (startPoint != null) {
            HiLog.info(hiLogLabel, "onDraw start Point = " + startPoint.toString() + " end point = "
                    + endPoint + " layout config width = " + width + " height = " + height);
            if (endPoint != null) {
                canvas.drawLine(startPoint, endPoint, paint);
            }
        }
    }
}
