package com.parkingwang.keyboard.view;

import com.parkingwang.keyboard.annotation.NonNull;
import ohos.agp.components.element.PixelMapElement;
import ohos.agp.render.Canvas;
import ohos.agp.render.Paint;
import ohos.agp.render.Path;
import ohos.agp.utils.Color;
import ohos.agp.utils.Rect;
import ohos.agp.utils.RectFloat;
import ohos.global.resource.Resource;
import ohos.hiviewdfx.HiLog;
import ohos.hiviewdfx.HiLogLabel;
import ohos.media.image.PixelMap;

/**
 * @author 黄浩杭 (huanghaohang@parkingwang.com)
 * @since 2018-07-24
 */
public class SelectedDrawable extends PixelMapElement {
    private HiLogLabel hiLogLabel = new HiLogLabel(HiLog.LOG_APP, 0x78002, SelectedDrawable.class.getSimpleName());

    protected float mRadius;
    protected Rect mRect = new Rect();
    protected Position mPosition = Position.FIRST;
    protected final Paint mPaint = new Paint();
    protected final Path mPath = new Path();
    protected final RectFloat mPathRectF = new RectFloat();

    public SelectedDrawable(PixelMap pixelMap) {
        super(pixelMap);
        mPaint.setStyle(Paint.Style.STROKE_STYLE);
        mPaint.setStrokeWidth(30);
    }

    public SelectedDrawable(Resource resource) {
        super(resource);
    }

    public SelectedDrawable(Resource resource, boolean startDecode) {
        super(resource, startDecode);
        mPaint.setStyle(Paint.Style.STROKE_STYLE);
        mPaint.setStrokeWidth(30);
        mPaint.setColor(Color.BLUE);
    }

    public void setRadius(float radius) {
        mRadius = radius;
    }

    public void setWidth(float width) {
        mPaint.setStrokeWidth(width);
    }

    public Rect getRect() {
        return mRect;
    }

    public void setPosition(@NonNull Position position) {
        mPosition = position;
    }

    public void setColor(int color) {
        mPaint.setColor(new Color(color));
    }

    public void drawCanvas(Canvas canvas) {
        HiLog.info(hiLogLabel, "drawCanvas rect = " + mRect.left + " canvas = " + canvas);
        float strokeWidthOffset = mPaint.getStrokeWidth() / 2;
        int left = mRect.left;
        int top = mRect.top + (int) strokeWidthOffset;
        int right = mRect.right - (int) strokeWidthOffset;
        int bottom = mRect.bottom - (int) strokeWidthOffset;
        final float[] radiusArray = new float[8];
        if (mPosition == Position.FIRST) {
            left += strokeWidthOffset;
            radiusArray[0] = mRadius;
            radiusArray[1] = mRadius;
            radiusArray[6] = mRadius;
            radiusArray[7] = mRadius;
        } else if (mPosition == Position.LAST) {
            right -= strokeWidthOffset;
            radiusArray[2] = mRadius;
            radiusArray[3] = mRadius;
            radiusArray[4] = mRadius;
            radiusArray[5] = mRadius;
        }
        HiLog.info(hiLogLabel, "drawCanvas path = " + mPath + " left" + left);
        mPath.reset();
        mPathRectF.clear();
        mPathRectF.fuse(left, top, right, bottom);
        HiLog.info(hiLogLabel, "drawCanvas path = " + mPath + " rectf" + mPathRectF.left);
        mPath.addRoundRect(mPathRectF, radiusArray, Path.Direction.CLOCK_WISE);
        canvas.drawPath(mPath, mPaint);
    }

    @Override
    public void setAlpha(int alpha) {
    }

    enum Position {
        FIRST,
        MIDDLE,
        LAST
    }
}
