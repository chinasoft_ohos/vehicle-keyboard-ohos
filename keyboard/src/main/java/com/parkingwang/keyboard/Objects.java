package com.parkingwang.keyboard;

/**
 * @author 陈哈哈 (yoojiachen@gmail.com)
 */
public class Objects {
    private Objects() {
    }

    public static <T> T notNull(T val) {
        if (val == null) {
            throw new NullPointerException("Null pointer");
        }
        return val;
    }
}
